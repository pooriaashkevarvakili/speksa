import { api } from "src/boot/axios"
const state = {
    file: []
}
const getters = {
    useGetters: (state) => {
        return state.cart
    }
}
const mutations = {

}
const actions = {
    async fetchfile(title) {
        const root_directory_id = localStorage.getItem('root_directory_id')
        const token = localStorage.getItem('token')

        api.post(`/directory/${root_directory_id}`, {
            title
        }, {


            headers: {
                Authorization: 'Bearer ' + token
            }
        }).then(res => {
            console.log(res.data)
            state.file = res.data


        })
    },
    async deleteFile() {
        const root_directory_id = localStorage.getItem('root_directory_id')
        const token = localStorage.getItem('token')

        api.delete(`/directory/${root_directory_id}`, {
            title
        }, {


            headers: {
                Authorization: 'Bearer ' + token
            }
        }).then(res => {
            console.log(res.data)
            state.file = res.data


        })
    }
}
export default {
    namespaced: true,
    state, getters, mutations, actions
}