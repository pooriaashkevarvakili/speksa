
import { api } from "src/boot/axios"
const state = {
    user: {}


}
const getters = {

}
const mutations = {

}
const actions = {
    async fetchUsers({ }, user) {
        const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InNhbWFkYW5pLm1haGRpQGdtYWlsLmNvbSIsImlhdCI6MTYyOTAyNjg1MCwiZXhwIjoxNjI5MDI3NzUwfQ.I0pMJqRUz2lFMHrU2gHmUiYQiS0CuvK_SvfhV9IKVp0"
        api.post('/user/login', {
            email: user.email,
            password: user.password,

        }).then(res => {
            console.log(res)
            if (res.data.data.access_token) {

                localStorage.setItem('token', res.data.data.access_token)
                localStorage.setItem('root_directory_id', res.data.data.root_directory_id)


                window.location.href = '#/home'
            }
        })
    }
}
export default {
    namespaced: true,
    state, getters, mutations, actions
}