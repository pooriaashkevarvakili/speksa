import { store } from 'quasar/wrappers'
import { createStore } from 'vuex'
import Login from './module-example/Login'
import File from './module-example/File'

export default store(function (/* { ssrContext } */) {
  const Store = createStore({
    modules: {
      Login,
      File

    },



    strict: process.env.DEBUGGING
  })

  return Store
})
